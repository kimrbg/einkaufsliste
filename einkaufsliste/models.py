from django.db import models

# Create your models here.

class Liste(models.Model):
    objects = None
    name = models.CharField(max_length=200)

    def erledigt(self):
        return Eintrag.objects.filter(liste=self, status=True).count()

    def anzahl(self):
        return Eintrag.objects.filter(liste=self).count()

    def __str__(self):
        return self.name

class Eintrag(models.Model):
    objects = None
    name = models.TextField()
    preis = models.FloatField(default=0.0)
    menge = models.IntegerField(default=1)
    geschaeft = models.ForeignKey(to="Geschaeft", on_delete=models.SET_NULL, null=True)
    liste = models.ForeignKey(to="Liste", on_delete=models.CASCADE)
    status = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Geschaeft(models.Model):
    objects = None
    name = models.CharField(max_length=200)
    oeffnungszeiten = models.TextField(default="")

    def __str__(self):
        return self.name
