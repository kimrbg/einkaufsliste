from django.shortcuts import render, redirect, get_object_or_404
from . import models

# Create your views here.


def index(request):
    listen = models.Liste.objects.all()
    geschaefte = models.Geschaeft.objects.all()
    context = {"listen": listen, "geschaefte": geschaefte}
    return render(request, "einkaufsliste/index.html", context)


def liste(request, liste_id):
    context = {}
    context["liste"] = get_object_or_404(models.Liste, pk=liste_id)
    context["eintraege"] = models.Eintrag.objects.filter(liste=liste_id)
    context["geschaefte"] = models.Geschaeft.objects.all()
    summe = 0
    for eintrag in context["eintraege"]:
        summe += eintrag.preis * eintrag.menge
    context["summe"] = summe
    if "aktuelles_geschaeft" in request.GET:
        try:
            context["aktuelles_geschaeft"] = int(request.GET["aktuelles_geschaeft"])
        except ValueError:
            pass

    return render(request, "einkaufsliste/list.html", context)


def geschaeft(request, geschaeft_id):
    context = {}
    context["geschaeft"] = get_object_or_404(models.Geschaeft, pk=geschaeft_id)
    context["eintraege"] = models.Eintrag.objects.filter(geschaeft=geschaeft_id)
    context["listen"] = models.Liste.objects.all()
    if "aktuelle_liste" in request.GET:
        try:
            context["aktuelle_liste"] = int(request.GET["aktuelle_liste"])
        except ValueError:
            pass

    return render(request, "einkaufsliste/geschaeft.html", context)

def geschaeft_anlegen(request):
    models.Geschaeft.objects.create(name=request.POST["geschaeft_name"])
    return redirect("index")

# Hilfsfunktion zum Speichern eines neuen Eintrags
def _eintrag_anlegen(post):
    liste = get_object_or_404(models.Liste, pk=post["eintrag_liste"])
    if post["eintrag_geschaeft"] == "None":
        geschaeft = None
    else:
        geschaeft = get_object_or_404(models.Geschaeft, pk=post["eintrag_geschaeft"])
    try:
        menge = int(post["eintrag_menge"]) 
    except ValueError:
        menge = 1
    return models.Eintrag.objects.create(
            name=post["eintrag_name"], 
            menge=menge,
            liste=liste, 
            geschaeft=geschaeft)

def liste_eintrag_anlegen(request):
    eintrag = _eintrag_anlegen(request.POST)
    response = redirect("liste", eintrag.liste.id)
    geschaeft_id = "None"
    if eintrag.geschaeft:
        geschaeft_id = eintrag.geschaeft.id
    response["Location"] = response["Location"] + "?aktuelles_geschaeft=" + str(geschaeft_id) 
    return response

def geschaeft_eintrag_anlegen(request):
    eintrag = _eintrag_anlegen(request.POST)
    response = redirect("geschaeft", eintrag.geschaeft.id)
    response["Location"] = response["Location"] + "?aktuelle_liste=" + str(eintrag.liste.id)
    return response


def _eintrag_aktualisieren(eintrag, post):
    if "eintrag_menge" in post:
        try:
            menge = int(post["eintrag_menge"]) 
        except ValueError:
            menge = 1
        eintrag.menge = menge
    if "eintrag_preis" in post:
        try:
            preis = float(post["eintrag_preis"]) 
        except ValueError:
            preis = 0
        eintrag.preis = preis
    if "eintrag_name" in post:
        eintrag.name = post["eintrag_name"]
    # ACHTUNG: Der Status muss IMMER mitgegeben werden, falls der Eintrag schon erledigt ist, sonst wird er beim Fehlen auf False gesetzt.
    # Das lässt sich nicht ohne weiteres ändern, da im POST bei nicht gesetzter Checkbox "eintrag_status" nicht vorhanden ist.
    eintrag.status = "eintrag_status" in post
    if "eintrag_liste" in post:
        eintrag.liste = get_object_or_404(models.Liste, pk=post["eintrag_liste"])
    if "eintrag_geschaeft" in post:
        if post["eintrag_geschaeft"]=="None":
            eintrag.geschaeft = None
        else:
            eintrag.geschaeft = get_object_or_404(models.Geschaeft, pk=post["eintrag_geschaeft"])
    eintrag.save()


def liste_eintrag_bearbeiten(request, eintrag_id):
    eintrag = get_object_or_404(models.Eintrag, pk=eintrag_id)
    liste = eintrag.liste
    if request.POST:
        _eintrag_aktualisieren(eintrag, request.POST)
        return redirect("liste", liste.id)
    listen = models.Liste.objects.all()
    geschaefte = models.Geschaeft.objects.all()
    context = {
            "listen": listen, 
            "geschaefte": geschaefte, 
            "eintrag": eintrag, 
            "aktuelle_view": "liste_eintrag_bearbeiten",
            "view_abbrechen": "liste",
            "id_abbrechen": eintrag.liste.id,
            }
    return render(request, "einkaufsliste/eintrag_bearbeiten.html", context)


def geschaeft_eintrag_bearbeiten(request, eintrag_id):
    eintrag = get_object_or_404(models.Eintrag, pk=eintrag_id)
    geschaeft = eintrag.geschaeft
    if request.POST:
        _eintrag_aktualisieren(eintrag, request.POST)
        return redirect("geschaeft", geschaeft.id)
    listen = models.Liste.objects.all()
    geschaefte = models.Geschaeft.objects.all()
    context = {
            "listen": listen, 
            "geschaefte": geschaefte, 
            "eintrag": eintrag, 
            "aktuelle_view": "geschaeft_eintrag_bearbeiten",
            "view_abbrechen": "geschaeft",
            "id_abbrechen": eintrag.geschaeft.id,
            }
    return render(request, "einkaufsliste/eintrag_bearbeiten.html", context)


def eintrag_loeschen(request, eintrag_id):
    eintrag = get_object_or_404(models.Eintrag, pk=eintrag_id)
    eintrag.delete()
    view = request.POST["view_abbrechen"]
    id = request.POST["id_abbrechen"]
    return redirect(view, id)

def geschaeft_bearbeiten(request, geschaeft_id):
    geschaeft = get_object_or_404(models.Geschaeft, pk=geschaeft_id)
    context = {
            "geschaeft": geschaeft
            }
    if "save" in request.POST:
        geschaeft.name = request.POST["geschaeft_name"]
        geschaeft.oeffnungszeiten = request.POST["geschaeft_oeffnungszeiten"]
        geschaeft.save()
        return redirect("geschaeft", geschaeft.id)
    elif "delete" in request.POST:
        if "confirmed" in request.POST:
            geschaeft.delete()
            return redirect("index")
        else:
            return render(request, "einkaufsliste/geschaeft_loeschen.html", context)
    return render(request, "einkaufsliste/geschaeft_bearbeiten.html", context)
