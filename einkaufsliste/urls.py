"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('liste/<int:liste_id>', views.liste, name='liste'),
    path('geschaeft/<int:geschaeft_id>', views.geschaeft, name='geschaeft'),
    path('geschaeft_bearbeiten/<int:geschaeft_id>', views.geschaeft_bearbeiten, name='geschaeft_bearbeiten'),
    path('geschaeft_anlegen', views.geschaeft_anlegen, name='geschaeft_anlegen'),
    path('liste_eintrag_anlegen', views.liste_eintrag_anlegen, name='liste_eintrag_anlegen'),
    path('geschaeft_eintrag_anlegen', views.geschaeft_eintrag_anlegen, name='geschaeft_eintrag_anlegen'),
    path('liste_eintrag_bearbeiten/<int:eintrag_id>', views.liste_eintrag_bearbeiten, name='liste_eintrag_bearbeiten'),
    path('geschaeft_eintrag_bearbeiten/<int:eintrag_id>', views.geschaeft_eintrag_bearbeiten, name='geschaeft_eintrag_bearbeiten'),
    path('eintrag_loeschen/<int:eintrag_id>', views.eintrag_loeschen, name='eintrag_loeschen'),
]
